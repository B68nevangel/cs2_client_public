// console.log("hello from register")

let registerForm = document.querySelector('#registerUser')

registerForm.addEventListener("submit", (e) => {
	e.preventDefault()
	let firstName = document.querySelector('#firstName').value
	let lastName= document.querySelector('#lastName').value
	let email = document.querySelector('#userEmail').value
	let mobileNumber = document.querySelector('#mobileNumber').value
	let password1 = document.querySelector('#password1').value
	let password2 = document.querySelector('#password2').value

console.log(firstName)
console.log(lastName)
console.log(email)
console.log(mobileNumber)
console.log(password1)
console.log(password2)		
	
	// let's create the validation to submit button when all fields are registered and if both passwords match
	if ((password1 !== '' && password2 !== '') && (password1 === password2) && (mobileNumber.length === 11)) {
	// if all requirements are met, then let's check for duplicate emails 
	fetch('http://localhost:4000/api/users/email-exists', {
			method: 'POST',
			headers: {
				'Content-Type': 'application/json'
			},
			body: JSON.stringify({	//method that converts a JS object value into a JSON string
				email: email
			})
		})
		.then(res => res.json())
		.then(data => {
			// we will now create a control structure to see if there are no duplicates found
			if(data === false){
				fetch('http://localhost:4000/api/users', {
					method: 'POST',
					headers: {
						'Content-Type': 'application/json'
					},
					body: JSON.stringify({
						firstName: firstName,
						lastName: lastName,
						email: email,
						password: password1,
						mobileNumber: mobileNumber
					})
				})
				.then(res => {
					return res.json()
				})
				.then(data => {
					console.log(data)
					// lets give a proper response if registration is a success
					if(data === true){
						alert("New Account Has Registered Successfully")
						//after confirmation of the alert window, let's direct the user to  the login page
						// 
						console.log("Successfully registered")
						window.location.replace("./login.html")
					} else {
						// alert("Something went wrong in the registration")
						console.log("Something went wrong")
					}
				})
			}
		})
	} else {
		alert("Something went wrong, check credentials!")
		console.log("Something went wrong")
	}
})